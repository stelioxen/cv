import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captures',
  templateUrl: './captures.component.html',
  styleUrls: ['./captures.component.scss']
})
export class CapturesComponent implements OnInit {


	myImages = [
		{
			preview: '/assets/img/gallery/preview_l/1.jpg',
			full: '/assets/img/gallery/raw/1.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/30.jpg',
			full: '/assets/img/gallery/raw/30.jpg',
			width: 165,
			height: 249
    },
   
    {
			preview: '/assets/img/gallery/preview_l/2.jpg',
			full: '/assets/img/gallery/raw/2.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/7.jpg',
			full: '/assets/img/gallery/raw/7.jpg',
			width: 166,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/3.jpg',
			full: '/assets/img/gallery/raw/3.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/4.jpg',
			full: '/assets/img/gallery/raw/4.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/5.jpg',
			full: '/assets/img/gallery/raw/5.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/6.jpg',
			full: '/assets/img/gallery/raw/6.jpg',
			width: 257,
			height: 169
    },
    {
			preview: '/assets/img/gallery/preview_l/18.jpg',
			full: '/assets/img/gallery/raw/18.jpg',
			width: 187,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/8.jpg',
			full: '/assets/img/gallery/raw/8.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/9.jpg',
			full: '/assets/img/gallery/raw/9.jpg',
			width: 250,
			height: 166
    },
    {
			preview: '/assets/img/gallery/preview_l/33.jpg',
			full: '/assets/img/gallery/raw/33.jpg',
			width: 165,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/10.jpg',
			full: '/assets/img/gallery/raw/10.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/11.jpg',
			full: '/assets/img/gallery/raw/11.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/12.jpg',
			full: '/assets/img/gallery/raw/12.jpg',
			width: 165,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/13.jpg',
			full: '/assets/img/gallery/raw/13.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/14.jpg',
			full: '/assets/img/gallery/raw/14.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/34.jpg',
			full: '/assets/img/gallery/raw/34.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/15.jpg',
			full: '/assets/img/gallery/raw/15.jpg',
			width: 166,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/16.jpg',
			full: '/assets/img/gallery/raw/16.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/17.jpg',
			full: '/assets/img/gallery/raw/17.jpg',
			width: 166,
			height: 250
    },
    
    {
			preview: '/assets/img/gallery/preview_l/19.jpg',
			full: '/assets/img/gallery/raw/19.jpg',
			width: 250,
			height: 165
    },
    
    {
			preview: '/assets/img/gallery/preview_l/21.jpg',
			full: '/assets/img/gallery/raw/21.jpg',
			width: 250,
			height: 165
    },
  
    {
			preview: '/assets/img/gallery/preview_l/23.jpg',
			full: '/assets/img/gallery/raw/23.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/25.jpg',
			full: '/assets/img/gallery/raw/25.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/26.jpg',
			full: '/assets/img/gallery/raw/26.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/27.jpg',
			full: '/assets/img/gallery/raw/27.jpg',
			width: 253,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/29.jpg',
			full: '/assets/img/gallery/raw/29.jpg',
			width: 166,
			height: 250
    },
    {
			preview: '/assets/img/gallery/preview_l/28.jpg',
			full: '/assets/img/gallery/raw/28.jpg',
			width: 250,
			height: 165
    },
    {
			preview: '/assets/img/gallery/preview_l/35.jpg',
			full: '/assets/img/gallery/raw/35.jpg',
			width: 167,
			height: 252
    },
    {
			preview: '/assets/img/gallery/preview_l/20.jpg',
			full: '/assets/img/gallery/raw/20.jpg',
			width: 295,
			height: 165
    },
   
   
    
    
  ]

  myConfig = {
		masonry: true,
		masonryMaxHeight: 195,
		masonryGutter: 6,
		loop: false,
		backgroundOpacity: 0.85,
		animationDuration: 100,
		counter: true,
		lightboxMaxHeight: '100vh - 86px',
		lightboxMaxWidth: '100%'
  } 
  constructor() {}

 
  ngOnInit() {
  }

}
