import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { SkillsComponent } from './skills/skills.component';
import { CapturesComponent } from './captures/captures.component';


const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent

  },
  {
    path: 'skills',
    component: SkillsComponent
  },
  {
    path: 'captures',
    component: CapturesComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
