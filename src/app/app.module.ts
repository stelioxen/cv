import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AboutComponent } from './about/about.component';
import { SkillsComponent } from './skills/skills.component';
import { CapturesComponent } from './captures/captures.component';
import { ParallaxModule, ParallaxConfig } from 'ngx-parallax';
import {CrystalGalleryModule} from 'ngx-crystal-gallery';
import { MoreComponent } from './more/more.component';
import { SocialComponent } from './social/social.component';
import { GraphicsComponent } from './graphics/graphics.component';
import { WebComponent } from './web/web.component';
import { EducationComponent } from './education/education.component';
import { RouterModule } from '@angular/router';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';



//import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';



@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    AboutComponent,
    SkillsComponent,
    CapturesComponent,
    MoreComponent,
    SocialComponent,
    GraphicsComponent,
    WebComponent,
    EducationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ParallaxModule,
    CrystalGalleryModule,
    RouterModule,
    BrowserModule,
    ScrollToModule.forRoot()

  ],

   // AnimateOnScrollModule.forRoot()


  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
