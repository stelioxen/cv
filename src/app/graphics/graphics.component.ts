import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.scss']
})
export class GraphicsComponent implements OnInit {

	myLogos1 = [
		{
			preview: '/assets/img/gallery/preview_l/1l.png',
			full: '/assets/img/gallery/raw/1l.png',
			width: 260,
			height: 158
    }]
    myLogos2 = [
    {
			preview: '/assets/img/gallery/preview_l/2l.jpg',
			full: '/assets/img/gallery/raw/2l.jpg',
	
    }]
   
    myLogos3 = [ {
			preview: '/assets/img/gallery/preview_l/4l.png',
			full: '/assets/img/gallery/raw/4l.png',
			width: 150,
			height: 130
    }]
    myLogos4 = [{
			preview: '/assets/img/gallery/preview_l/3l.jpg',
			full: '/assets/img/gallery/raw/3l.jpg',
			width: 450,
			height: 140
    }]
    myLogos5 = [{
			preview: '/assets/img/gallery/preview_l/6l.png',
			full: '/assets/img/gallery/raw/6l.png',
			width: 450,
			height: 150
    }]
 
   
   
    
    
  

  myConfig2 = {
		masonry: true,
		masonryMaxHeight: 195,
		masonryGutter: 6,
		loop: false,
		backgroundOpacity: 0.85,
		animationDuration: 100,
    counter: true,
		lightboxMaxHeight: '100vh - 86px',
		lightboxMaxWidth: '100%'
  } 
  constructor() { }

  ngOnInit() {
  }

  showLogo1(){
    document.getElementById("graphicsLeftTitle").style.display="none";
    if( document.getElementById("logo2").style.display=="block"){
      document.getElementById("logo2").style.display="none"
    }
    if( document.getElementById("logo3").style.display=="block"){
      document.getElementById("logo3").style.display="none"
    }
    if( document.getElementById("logo4").style.display=="block"){
      document.getElementById("logo4").style.display="none"
    }
    if( document.getElementById("logo5").style.display=="block"){
      document.getElementById("logo5").style.display="none"
    }
    document.getElementById("logo1").style.display="block";
  }
  showLogo2(){
    document.getElementById("graphicsLeftTitle").style.display="none";
    if( document.getElementById("logo1").style.display=="block"){
      document.getElementById("logo1").style.display="none"
    }
    if( document.getElementById("logo3").style.display=="block"){
      document.getElementById("logo3").style.display="none"
    }
    if( document.getElementById("logo4").style.display=="block"){
      document.getElementById("logo4").style.display="none"
    }
    if( document.getElementById("logo5").style.display=="block"){
      document.getElementById("logo5").style.display="none"
    }
    document.getElementById("logo2").style.display="block";
  }
  showLogo3(){
    document.getElementById("graphicsLeftTitle").style.display="none";
    if( document.getElementById("logo2").style.display=="block"){
      document.getElementById("logo2").style.display="none"
    }
    if( document.getElementById("logo1").style.display=="block"){
      document.getElementById("logo1").style.display="none"
    }
    if( document.getElementById("logo4").style.display=="block"){
      document.getElementById("logo4").style.display="none"
    }
    if( document.getElementById("logo5").style.display=="block"){
      document.getElementById("logo5").style.display="none"
    }
    document.getElementById("logo3").style.display="block";
  }
  showLogo4(){
    document.getElementById("graphicsLeftTitle").style.display="none";
    if( document.getElementById("logo2").style.display=="block"){
      document.getElementById("logo2").style.display="none"
    }
    if( document.getElementById("logo3").style.display=="block"){
      document.getElementById("logo3").style.display="none"
    }
    if( document.getElementById("logo1").style.display=="block"){
      document.getElementById("logo1").style.display="none"
    }
    if( document.getElementById("logo5").style.display=="block"){
      document.getElementById("logo5").style.display="none"
    }
    document.getElementById("logo4").style.display="block";
  }
  showLogo5(){
    document.getElementById("graphicsLeftTitle").style.display="none";
    if( document.getElementById("logo2").style.display=="block"){
      document.getElementById("logo2").style.display="none"
    }
    if( document.getElementById("logo3").style.display=="block"){
      document.getElementById("logo3").style.display="none"
    }
    if( document.getElementById("logo4").style.display=="block"){
      document.getElementById("logo4").style.display="none"
    }
    if( document.getElementById("logo1").style.display=="block"){
      document.getElementById("logo1").style.display="none"
    }
    document.getElementById("logo5").style.display="block";
  }

}
