import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {

  currentUrl: string;
  firstclick: boolean=true;


  constructor(private router: Router) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  
 
  ngOnInit() {}

  

  showmenu(){
    if(this.firstclick==true){
      document.getElementById("sidebarMenu").style.display="block";
      this.firstclick=false;
    }
    else if(this.firstclick==false){
      document.getElementById("sidebarMenu").style.display="none";

      this.firstclick=true;
    }
  }
  hidemenu(){
    
      document.getElementById("sidebarMenu").style.display="none";
      document.getElementById("sidebar").style.background="transparent";

  }

}
